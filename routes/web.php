<?php

use App\Models\Penjual;
use App\Models\KoleksiCupang;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\PenjualController;
use App\Http\Controllers\KoleksiCupangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/home',[HomeController::class, 'index'])->name('home.index');

    Route::get('/koleksicupang/cari',[KoleksiCupangController::class, 'search'])->name('koleksicupang.search');
    Route::get('/penjual/cari',[PenjualController::class, 'search'])->name('penjual.search');
    Route::resource('/gallery', GalleryController::class);
    Route::resource('/koleksicupang', KoleksiCupangController::class);
    Route::resource('/penjual', PenjualController::class);
});