<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\PenjualController;
use App\Http\Controllers\Api\KoleksiCupangController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('koleksicupang', [KoleksiCupangController::class, 'getAllKoleksiCupang']);
    Route::post('koleksicupang', [KoleksiCupangController::class, 'createKoleksiCupang']);
    Route::get('koleksicupang/{id}', [KoleksiCupangController::class, 'showCupang']);
    Route::put('koleksicupang/{id}', [KoleksiCupangController::class, 'updateKoleksiCupang']);
    Route::delete('koleksicupang/{id}', [KoleksiCupangController::class, 'deleteKoleksiCupang']); 

    Route::get('gallery', [GalleryController::class, 'getCupang']);
    Route::get('gallery/{id}', [GalleryController::class, 'showCupang']);

    Route::resource('/penjual', PenjualController::class);
    Route::post('logout', [AuthController::class, 'logout']);
});

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);