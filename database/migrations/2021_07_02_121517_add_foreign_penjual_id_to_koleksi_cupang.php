<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignPenjualIdToKoleksiCupang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('koleksi_cupang', function (Blueprint $table) {
            $table->unsignedBigInteger('penjual_id');
            $table->foreign('penjual_id')->references('id')->on('penjual')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('koleksi_cupang', function (Blueprint $table) {
            $table->dropForeign('penjual_id');
            $table->dropColumn('penjual_id');
        });
    }
}
