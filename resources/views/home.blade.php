@extends('layouts.app')
@section('content')
    <div class="panel-header bg-primary-gradient">
        <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                <div>
                    <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                    <h5 class="text-white op-7 mb-2">Welcome {{ Auth::user()->name }}!</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="page-inner">
        <div class="row" style="margin-top: -60px">
            <div class="col-sm-6 col-md-4">
                <div class="card card-stats card-primary card-round">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <div class="icon-big text-center">
                                    <i class="flaticon-box"></i>
                                </div>
                            </div>
                            <div class="col-7 col-stats">
                                <div class="numbers">
                                    <p class="card-category">Koleksi Saya</p>
                                    <h4 class="card-title">{{ $koleksi }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card card-stats card-info card-round">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <div class="icon-big text-center">
                                    <i class="flaticon-interface-6"></i>
                                </div>
                            </div>
                            <div class="col-7 col-stats">
                                <div class="numbers">
                                    <p class="card-category">Jumlah Penjual</p>
                                    <h4 class="card-title">{{ $penjual }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="card card-stats card-success card-round">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5">
                                <div class="icon-big text-center">
                                    <i class="flaticon-analytics"></i>
                                </div>
                            </div>
                            <div class="col-7 col-stats">
                                <div class="numbers">
                                    <p class="card-category">Total Asset Cupang</p>
                                    <h4 class="card-title">Rp. {{ $total }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="yearly" width="600"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="monthly" width="600"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script>
        let year = <?php echo $year; ?>;
        let yearly = <?php echo $yearly; ?>;
        let barChartData = {
            labels: year,
            datasets: [{
                label: 'Koleksi Cupang',
                backgroundColor: "pink",
                data: yearly
            }]
        };

        let ctx = document.getElementById("yearly").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Yearly Cupang Added'
                }
            }
        });

        let month = <?php echo $month; ?>;
        let monthly = <?php echo $monthly; ?>;
        let chartData = {
            labels: month,
            datasets: [{
                label: 'Koleksi Cupang',
                backgroundColor: "lightblue",
                data: monthly
            }]
        };

        let ctxx = document.getElementById("monthly").getContext("2d");
        window.myBar = new Chart(ctxx, {
            type: 'line',
            data: chartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Monthly Cupang Added'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 1
                        },
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });
    </script>

@endpush
