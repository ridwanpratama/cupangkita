<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

    <div class="container-fluid">

        <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
            <li class="nav-item dropdown hidden-caret">
                <span class="text-white">Hi, {{ Auth::user()->name }}</span>
            </li>
        </ul>
    </div>
</nav>