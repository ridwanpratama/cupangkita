<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <ul class="nav nav-primary">

                <li class="nav-item sidebar-item {{ set_active('home') }}">
                    <a href="{{ route('home.index') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item sidebar-item {{ set_active(['koleksicupang.index', 'koleksicupang.edit', 'koleksicupang.create']) }}" href="{{ route('koleksicupang.index') }}">
                    <a href="{{ route('koleksicupang.index') }}">
                        <i class="fas fa-fish"></i>
                        <p>Manage Cupang Saya</p>
                    </a>
                </li>

                <li class="nav-item sidebar-item {{ set_active(['gallery.index', 'gallery.create', 'gallery.edit']) }}">
                    <a href="{{ route('gallery.index') }}">
                        <i class="fas fa-images"></i>
                        <p>Gallery Cupang</p>
                    </a>
                </li>

                <li class="nav-item sidebar-item {{ set_active(['penjual.index', 'penjual.create']) }}">
                    <a href="{{ route('penjual.index') }}">
                        <i class="fas fa-people-carry"></i>
                        <p>Data Penjual</p>
                    </a>
                
                <li class="nav-item">
                    <a href="" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
           
            </ul>
        </div>
    </div>
</div>
