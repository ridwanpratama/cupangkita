@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Penjual</h4>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <a href="{{ route('penjual.create') }}" class="btn btn-md btn-primary">
                    + Data Penjual
                </a>
            </div>
            <div class="col-md-8 form-inline justify-content-end">
                <form action="{{ route('penjual.search') }}" method="GET">
                    <input type="text" class="form-control" name="keyword" placeholder="Cari Penjual .." value="">
                    <input type="submit" class="btn btn-secondary" value="Search">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Data Penjual</div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nama Penjual</th>
                                        <th scope="col">No Telp</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($penjual as $item)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>
                                            <td>{{ $item->nama_penjual }}</td>
                                            <td>{{ $item->no_telp }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->alamat }}</td>
                                            <td>
                                                <a href="{{ route('penjual.edit', $item->id) }}" class="btn btn-info btn-sm"><i
                                                        class="fas fa-pencil-alt"></i></a>
                                                        <form action="{{ route('penjual.destroy', $item->id) }}" method="POST" class="d-inline">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>
                                            </td>
                                        </tr>
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            Tidak Ada Data
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
