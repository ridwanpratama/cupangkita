@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Penjual</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Tambah Data</div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('penjual.update', $penjual->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="nama_penjual">Nama Penjual</label>
                                <input type="text" class="form-control" name="nama_penjual" id="nama_penjual"
                                    placeholder="Masukkan Nama Penjual..." value="{{ $penjual->nama_penjual }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="no_telp">No Telp</label>
                                <input type="number" class="form-control" name="no_telp" id="no_telp"
                                    placeholder="Masukkan No Telp..." value="{{ $penjual->no_telp }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="{{ $penjual->email }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" id="alamat" name="alamat" rows="3"
                                    placeholder="Masukkan Alamat Penjual..." required>{{ $penjual->alamat }}</textarea>
                            </div>
                    
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
