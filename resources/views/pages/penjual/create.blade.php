@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Penjual</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Tambah Data</div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('penjual.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="nama_penjual">Nama Penjual</label>
                                <input type="text" class="form-control" name="nama_penjual" id="nama_penjual"
                                    placeholder="Masukkan Nama Penjual..." required>
                            </div>
                    
                            <div class="form-group">
                                <label for="no_telp">No Telp</label>
                                <input type="number" class="form-control" name="no_telp" id="no_telp"
                                    placeholder="Masukkan No Telp..." required>
                            </div>
                    
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" required>
                            </div>
                    
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea class="form-control" id="alamat" name="alamat" rows="3"
                                    placeholder="Masukkan Alamat Penjual..." required></textarea>
                            </div>
                    
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
