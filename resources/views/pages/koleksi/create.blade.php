@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Koleksi</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Tambah Data</div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('koleksicupang.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="nama_cupang">Nama Cupang</label>
                                <input type="text" class="form-control" name="nama_cupang" id="nama_cupang"
                                    placeholder="Masukkan Nama cupang..." required>
                            </div>

                            <div class="form-group">
                                <label for="jenis_cupang">Jenis Cupang</label>
                                <input type="text" class="form-control" name="jenis_cupang" id="jenis_cupang"
                                    placeholder="Masukkan Jenis cupang..." required>
                            </div>

                            <div class="form-group">
                                <label for="image">Gambar</label>
                                <input type="file" class="form-control-file" id="image" name="image" required>
                            </div>

                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="number" class="form-control" name="harga" id="harga"
                                    placeholder="Masukkan Harga..." required>
                            </div>

                            <div class="form-group">
                                <label for="penjual_id">Penjual</label>
                                <select name="penjual_id" id="penjual_id" required class="form-control">
                                    <option value disable>Pilih Penjual</option>
                                    @foreach ($penjual as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_penjual }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="tanggal_beli">Tanggal Beli</label>
                                <input type="date" class="form-control" name="tanggal_beli" id="tanggal_beli" required>
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"
                                    placeholder="Masukkan Deskripsi Cupang..." required></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
