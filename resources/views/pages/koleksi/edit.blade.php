@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Master</h4>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Card Header</div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('koleksicupang.update', $koleksi->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="nama_cupang">Nama Cupang</label>
                                <input type="text" class="form-control" name="nama_cupang" id="nama_cupang"
                                    placeholder="Masukkan Nama cupang..." value="{{ $koleksi->nama_cupang }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="jenis_cupang">Jenis Cupang</label>
                                <input type="text" class="form-control" name="jenis_cupang" id="jenis_cupang"
                                    placeholder="Masukkan Jenis cupang..." value="{{ $koleksi->jenis_cupang }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="image">Gambar</label>
                                <input type="file" class="form-control-file" id="image" name="image">
                            </div>
                    
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <input type="number" class="form-control" name="harga" id="harga" value="{{ $koleksi->harga }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="penjual_id">Penjual</label>
                                <select name="penjual_id" id="penjual_id" required class="form-control">
                                     <option value={{ $koleksi->penjual_id }}>{{ $koleksi->penjual->nama_penjual }}</option>
                                     @foreach ($penjual as $item)
                                         <option value="{{ $item->id }}">{{ $item->nama_penjual }}</option>
                                     @endforeach
                                </select>
                            </div>
                    
                            <div class="form-group">
                                <label for="tanggal_beli">Tanggal Beli</label>
                                <input type="date" class="form-control" name="tanggal_beli" id="tanggal_beli" value="{{ $koleksi->tanggal_beli }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3">{{ $koleksi->deskripsi }}</textarea>
                            </div>
                    
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
