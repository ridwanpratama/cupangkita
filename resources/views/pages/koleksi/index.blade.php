@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Data Cupang</h4>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <a href="{{ route('koleksicupang.create') }}" class="btn btn-md btn-primary">
                    + Data Cupang
                </a>
            </div>
            <div class="col-md-8 form-inline justify-content-end">
                <form action="{{ route('koleksicupang.search') }}" method="GET">
                    <input type="text" class="form-control" name="keyword" placeholder="Cari Cupang .." value="">
                    <input type="submit" class="btn btn-secondary" value="Search">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Data Cupang</div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive mt-2"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nama Cupang</th>
                                        <th scope="col">Jenis Cupang</th>
                                        <th scope="col">Gambar</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Penjual</th>
                                        <th scope="col">Tanggal Beli</th>
                                        <th scope="col">Deskripsi</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($koleksi as $item)
                                        <tr>
                                            <th scope="row">{{ ++$i }}</th>
                                            <td>{{ $item->nama_cupang }}</td>
                                            <td>{{ $item->jenis_cupang }}</td>
                                            <td><img src="{{ Storage::url('assets/image/'.$item->image) }}" alt="image" width="150px"
                                                    class="img-fluid"></td>
                                            <td>{{ $item->harga }}</td>
                                            <td>{{ $item->penjual->nama_penjual }}</td>
                                            <td>{{ $item->tanggal_beli }}</td>
                                            <td>{{ Str::limit($item->deskripsi, 25) }}</td>
                                            <td>
                                                <a href="{{ route('koleksicupang.edit', $item->id) }}" class="btn btn-info btn-sm mt-2 mb-2"><i
                                                        class="fas fa-pencil-alt"></i></a>
                                                <form action="{{ route('koleksicupang.destroy', $item->id) }}" method="POST" class="d-inline">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm mb-2" onclick="return confirm('Hapus data?')">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            Tidak Ada Data
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                                {{ $koleksi->links() }}
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
