@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Gallery</h4>
        </div>
        <div class="row mb-3">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Koleksi Saya</div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @forelse ($cupang as $data)
                                <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">
                                    <a style="display:inline-block;" href="{{ route('gallery.show', $data->id) }}">
                                    <img src="{{ Storage::url('assets/image/'.$data->image) }}"
                                        class="w-100 shadow-1-strong rounded mb-4" /></a>
                                    <a href="{{ route('gallery.show', $data->id) }}" class="stretched-link"></a>
                                </div>
                            @empty
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <p>Tidak ada data.</p>
                                    </div>
                                </div>
                            @endforelse
                        </div>
                        {{ $cupang->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
