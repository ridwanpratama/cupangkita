@extends('layouts.app')
@section('content')
    <div class="page-inner">
        <div class="page-header">
            <h4 class="page-title">Detail</h4>
        </div>
        <div class="row mb-3">

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <img src="{{ Storage::url('assets/image/'.$cupang->image) }}" class="img-fluid" width="100%">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Nama Cupang: {{ $cupang->nama_cupang }}</li>
                    <li class="list-group-item">Jenis Cupang: {{ $cupang->jenis_cupang }}</li>
                    <li class="list-group-item">Harga Cupang: {{ $cupang->harga }}</li>
                    <li class="list-group-item">Penjual: {{ $cupang->penjual->nama_penjual }}</li>
                    <li class="list-group-item">Tanggal Beli: {{ $cupang->tanggal_beli }}</li>
                    <li class="list-group-item">Deskripsi: {{ $cupang->deskripsi }}</li>
                  </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
