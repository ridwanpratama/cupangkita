<!DOCTYPE html>
<html>

<head>
    <title>CupangKita</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/welcome.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
</head>

<body>

    
    <div class="wrapper">
        <h1>CupangKita</h1>
        <br>
        @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}"> Login | </a>

                        @if (Route::has('register'))
                        <a href="{{ route('register') }}"> Register</a>
                        @endif
                    @endauth
        @endif
        <p>Manage Koleksi Cupang Anda</p>
      
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
        <div><span class="dot"></span></div>
    </div>

</body>

</html>
