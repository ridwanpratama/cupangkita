<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\KoleksiCupang;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CupangTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function test_only_logged_in_users_can_access_home()
    {
        $response = $this->get('/home')->assertRedirect('/login');
    }

    /** @test */
    public function test_only_logged_in_users_can_access_gallery()
    {
        $response = $this->get('/gallery')->assertRedirect('/login');
    }

    /** @test */
    public function test_only_logged_in_users_can_manage_cupang()
    {
        $response = $this->get('/koleksicupang')->assertRedirect('/login');
    }

    /** @test */
    public function test_authenticated_users_can_access_home()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/home')->assertOk();
    }

    /** @test */
    public function test_authenticated_users_can_access_gallery()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/gallery')->assertOk();
    }

    /** @test */
    public function test_authenticated_users_can_create_cupang()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $response = $this->get('/koleksicupang/create')->assertOk();
    }
}
