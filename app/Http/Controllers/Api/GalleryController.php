<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\KoleksiCupang;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    public function getCupang()
    {
        $data = KoleksiCupang::where('users_id', Auth::user()->id)->get();
        return response()->json([
            'success' => true,
            'message' => 'List Gallery Cupang',
            'data' => $data,
        ], 200);
    }

    public function showCupang($id)
    {
        $koleksi = KoleksiCupang::find($id);
        if ($koleksi) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Cupang',
                'data' => $koleksi,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Cupang tidak ditemukan',
            ], 404);
        }
    }
}
