<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\KoleksiCupang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KoleksiCupangController extends Controller
{
    public function getAllKoleksiCupang()
    {
        $koleksi = KoleksiCupang::all();
        return response()->json([
            'success' => true,
            'message' => 'List Semua Cupang',
            'data' => $koleksi,
        ], 200);
    }

    public function showCupang($id)
    {
        $koleksi = KoleksiCupang::find($id);
        if ($koleksi) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Cupang',
                'data' => $koleksi,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Cupang tidak ditemukan',
            ], 404);
        }
    }

    public function createKoleksiCupang(Request $request)
    {
        $data = $request->all();
        if ($request->hasfile('image')) {
            $imageName = rand(1000, 9999) . '-' . date('Y-m-d') . '-' . Auth::user()->id . '-' . $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('assets/image', $imageName, 'public');
            $data['image'] = $imageName;
            $data['users_id'] = Auth::user()->id;

            KoleksiCupang::create($data);
        }

        return response()->json([
            "success" => true,
            "message" => "Data Berhasil Disimpan!",
        ], 201);
    }

    public function updateKoleksiCupang(Request $request, $id)
    {
        if (KoleksiCupang::where('id', $id)->exists()) {
            $koleksi = KoleksiCupang::find($id);
            $data = $request->all();
            if ($request->hasfile('image')) {
                $imageName = rand(1000, 9999) . '-' . date('Y-m-d') . '-' . Auth::user()->id . '-' . $request->file('image')->getClientOriginalName();
                $request->file('image')->storeAs('assets/image', $imageName, 'public');
                $data['image'] = $imageName;

                $path = $koleksi->image;
                Storage::delete('public/assets/image/' . $path);
            }
            $data['users_id'] = Auth::user()->id;
            $koleksi->update($data);

            return response()->json([
                "success" => true,
                "message" => "Data berhasil diupdate",
            ], 200);
        } else {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ], 404);
        }
    }

    public function deleteKoleksiCupang($id)
    {
        if (KoleksiCupang::where('id', $id)->exists()) {
            $koleksi = KoleksiCupang::find($id);
            $koleksi->delete();

            return response()->json([
                "success" => true,
                "message" => "Data berhasil dihapus",
            ], 202);
        } else {
            return response()->json([
                "success" => false,
                "message" => "Data tidak ditemukan",
            ], 404);
        }
    }
}
