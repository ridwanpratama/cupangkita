<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Penjual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PenjualController extends Controller
{
    public function index()
    {
        $penjual = Penjual::where('users_id', Auth::user()->id)->get();
        return response()->json([
            'success' => true,
            'message' => 'List Semua Cupang',
            'data' => $penjual,
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_penjual' => 'required',
            'no_telp' => 'required|min:10',
            'email' => 'required|email',
            'alamat' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Semua Kolom Wajib Diisi!',
                'data' => $validator->errors(),
            ], 401);
        } else {
            $penjual = Penjual::create([
                'nama_penjual' => $request->input('nama_penjual'),
                'no_telp' => $request->input('no_telp'),
                'email' => $request->input('email'),
                'alamat' => $request->input('alamat'),
                'users_id' => Auth::user()->id,
            ]);

            if ($penjual) {
                return response()->json([
                    'success' => true,
                    'message' => 'Penjual Berhasil Disimpan!',
                    'data' => $penjual,
                ], 201);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Penjual Gagal Disimpan!',
                ], 400);
            }
        }
    }

    public function show($id)
    {
        $penjual = Penjual::find($id);

        if ($penjual) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Penjual!',
                'data' => $penjual,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Penjual Tidak Ditemukan!',
            ], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_penjual' => 'required',
            'no_telp' => 'required|min:10',
            'email' => 'required|email',
            'alamat' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Semua Kolom Wajib Diisi!',
                'data' => $validator->errors(),
            ], 401);
        } else {
            $penjual = Penjual::whereId($id)->update([
                'nama_penjual' => $request->input('nama_penjual'),
                'no_telp' => $request->input('no_telp'),
                'email' => $request->input('email'),
                'alamat' => $request->input('alamat'),
                'users_id' => Auth::user()->id,
            ]);

            if ($penjual) {
                return response()->json([
                    'success' => true,
                    'message' => 'Penjual Berhasil Diupdate!',
                    'data' => $penjual,
                ], 201);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Penjual Gagal Diupdate!',
                ], 400);
            }

        }
    }

    public function destroy($id)
    {
        $penjual = Penjual::whereId($id)->first();
        $penjual->delete();

        if ($penjual) {
            return response()->json([
                'success' => true,
                'message' => 'Penjual Berhasil Dihapus!',
            ], 200);
        }
    }
}
