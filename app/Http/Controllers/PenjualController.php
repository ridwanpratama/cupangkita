<?php

namespace App\Http\Controllers;

use App\Models\Penjual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PenjualRequest;
use App\Models\KoleksiCupang;

class PenjualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjual = Penjual::where('users_id', Auth::user()->id)->orderBy('id','DESC')->paginate(5);
        return view('pages.penjual.index', compact('penjual'))->with('i',(request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.penjual.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenjualRequest $request)
    {
        $data = $request->all();
        $data['users_id'] = Auth::user()->id;
        Penjual::create($data);
        
        return redirect()->route('penjual.index')->with('toast_success', 'Data berhasil disimpan');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penjual = Penjual::findorFail($id);
        return view('pages.penjual.edit', compact('penjual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenjualRequest $request, $id)
    {
        $data = $request->all();
        $item =  Penjual::findorFail($id);
        $item->update($data);

        return redirect()->route('penjual.index')->with('toast_success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Penjual::findorFail($id);
        $data->delete();

        return redirect()->route('penjual.index')->with('toast_success', 'Data berhasil dihapus');
    }

    public function search(Request $request)
    {
		$keyword = $request->keyword;
        $penjual = Penjual::where('nama_penjual','like',"%".$keyword."%")->paginate(5);

        return view('pages.penjual.index', compact('penjual'))->with('i',(request()->input('page', 1) - 1) * 5);
    }
}
