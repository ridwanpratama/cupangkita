<?php

namespace App\Http\Controllers;

use App\Models\KoleksiCupang;
use App\Models\Penjual;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        // Chart Yearly
        $year = [];
        for ($y = 2018, $now = date('Y'); $y <= $now; ++$y) {
            $year[] = $y;
        }

        $yearly = [];
        foreach ($year as $key => $value) {
            $yearly[] = KoleksiCupang::where(\DB::raw("DATE_FORMAT(tanggal_beli, '%Y')"), $value)
                ->where('users_id', Auth::user()->id)->count();
        }

        // Chart Monthly
        $month = ['January','February','March','April','May','June','July','August','September','October','November','December'];

        $monthly = [];
        foreach ($month as $key => $value) {
            $monthly[] = KoleksiCupang::where(\DB::raw("DATE_FORMAT(tanggal_beli, '%M')"),$value)
                ->where('users_id', Auth::user()->id)->count();
        }

        // Statistic Card
        $data = [
            'koleksi' => KoleksiCupang::all()->count(),
            'penjual' => Penjual::all()->count(),
            'total' => KoleksiCupang::sum('harga'),
        ];

        return view('home')->with($data)
            ->with('year', json_encode($year, JSON_NUMERIC_CHECK))
            ->with('yearly', json_encode($yearly, JSON_NUMERIC_CHECK))
            ->with('month', json_encode($month, JSON_NUMERIC_CHECK))
            ->with('monthly', json_encode($monthly, JSON_NUMERIC_CHECK));
    }
}
