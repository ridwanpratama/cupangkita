<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KoleksiCupang;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    public function index()
    {
        $cupang = KoleksiCupang::where('users_id', Auth::user()->id)->orderBy('id','DESC')->paginate(12);
        $i = KoleksiCupang::pluck('id');
        return view('pages.gallery.index', compact('cupang'))->with('i',(request()->input('page', 1) - 1) * 12);
    }
    
    public function show($id)
    {
        $cupang = KoleksiCupang::findorFail($id);
        return view('pages.gallery.show', compact('cupang'));
    }
}
