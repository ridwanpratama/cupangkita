<?php

namespace App\Http\Controllers;

use App\Http\Requests\KoleksiCupangRequest;
use App\Models\KoleksiCupang;
use App\Models\Penjual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class KoleksiCupangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $koleksi = KoleksiCupang::where('users_id', Auth::user()->id)->orderBy('id', 'DESC')->paginate(5);
        return view('pages.koleksi.index', compact('koleksi'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penjual = Penjual::all();
        return view('pages.koleksi.create', compact('penjual'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KoleksiCupangRequest $request)
    {
        $data = $request->all();
        if ($request->hasfile('image')) {
            $imageName = rand(1000, 9999) . '-' . date('Y-m-d') . '-' . Auth::user()->id . '-' . $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('assets/image', $imageName, 'public');
            $data['image'] = $imageName;
            $data['users_id'] = Auth::user()->id;
            
            KoleksiCupang::create($data);

            return redirect()->route('koleksicupang.index')->with('toast_success', 'Data berhasil disimpan');
        } else {
            return redirect()->route('koleksicupang.index')->with('toast_danger', 'Data gagal disimpan');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $koleksi = KoleksiCupang::findorFail($id);
        $penjual = Penjual::all();
        return view('pages.koleksi.edit', compact('koleksi', 'penjual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_cupang' => 'required',
            'jenis_cupang' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg',
            'harga' => 'required|integer',
            'penjual_id' => 'required',
            'tanggal_beli' => 'required',
            'deskripsi' => 'required|min:3',
        ]);

        $koleksi = KoleksiCupang::findorFail($id);
        $data = $request->all();
        if ($request->hasfile('image')) {
            $imageName = rand(1000, 9999) . '-' . date('Y-m-d') . '-' . Auth::user()->id . '-' . $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('assets/image', $imageName, 'public');
            $data['image'] = $imageName;
            
            $path = $koleksi->image;
            Storage::delete('public/assets/image/'.$path);
        }
        $data['users_id'] = Auth::user()->id;
        $koleksi->update($data);

        return redirect()->route('koleksicupang.index')->with('toast_success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = KoleksiCupang::findorFail($id);
        $path = $data->image;
        Storage::delete('public/assets/image/'.$path);
        $data->delete();
        
        return redirect()->route('koleksicupang.index')->with('toast_success', 'Data berhasil dihapus');
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $koleksi = KoleksiCupang::where('nama_cupang', 'like', "%" . $keyword . "%")->paginate(5);

        return view('pages.koleksi.index', compact('koleksi'))->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
