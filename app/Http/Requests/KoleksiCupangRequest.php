<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KoleksiCupangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_cupang' => 'required',
            'jenis_cupang' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'harga' => 'required|integer',
            'penjual_id' => 'required',
            'tanggal_beli' => 'required',
            'deskripsi' => 'required|min:3',
        ];
    }
}
