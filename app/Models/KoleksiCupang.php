<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KoleksiCupang extends Model
{
    use HasFactory;

    protected $table = 'koleksi_cupang';
    protected $guarded = [];

    public function user(){
        return $this->hasMany('App\Models\User');
    }

    public function penjual(){
        return $this->belongsTo('App\Models\Penjual');
    }
}
