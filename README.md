# CupangKita

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
CupangKita adalah aplikasi berbasis web yang digunakan untuk membantu permasalahan kolektor ikan cupang dalam memanage koleksi cupangnya.
	
## Technologies
Aplikasi ini dibuat dengan:
* Laravel 8
* MySQL
* Bootstrap

## Setup

1. composer install
2. composer dump-autoload
3. php artisan migrate
4. php artisan serve

Mailtrap:
email: codesekai82@gmail.com
password: @Codesekai

## Contact Info

- [WhatsApp](https://api.whatsapp.com/send/?phone=6289695869902&text&app_absent=0).
